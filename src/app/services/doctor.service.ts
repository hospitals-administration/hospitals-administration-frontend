import { Injectable } from '@angular/core';
// SERVER CONFIG
import SERVER from '../config/server.config';
import { AuthService } from './auth.service';
// SERVICES
import { WebService } from './web.service';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  public options: any;

  constructor(
    private webService: WebService,
    private authService: AuthService
  ) {
    this.options = this.webService.JSONOptions(this.authService.getToken());
   }

  findAll(page?: any, size?: any) {
    return this.webService.get(SERVER.DOCTORS + this.webService.pageSize(page, size), this.webService.JSONWhitHeaders());
  }

  findAllByHospitalId(page?: any, size?: any, hospitalId?: any) {
    return this.webService.get(SERVER.DOCTORS + '/hospital/' + hospitalId + this.webService.pageSize(page, size), this.webService.JSONWhitHeaders());
  }

  create(doctor: any) {
    return this.webService.post(SERVER.DOCTORS, doctor, this.options);
  }

  update(doctor: any) {
    return this.webService.put(SERVER.DOCTORS, doctor, this.options);
  }

  findOne(doctorId: any) {
    return this.webService.get(SERVER.DOCTORS + '/' + doctorId, this.options);
  }

  deleteOne(doctorId: any) {
    return this.webService.delete(SERVER.DOCTORS + '/' + doctorId, this.options);
  }

  // SEARCH
  search(name?: any, date?: any, page?: any, size?: any) {
    let response = null;
    if (name !== undefined && date === undefined) {
      response = '?name=' + name;
    }
    if (name === undefined && date !== undefined) {
      response = '?createdate=' + date;
    }
    if (name !== undefined && date !== undefined) {
      response = '?name=' + name + '&createdate=' + date;
    }
    response = response !== null ? response + this.webService.pageSize(page, size).replace('?', '&') : response === null? '' : response + this.webService.pageSize(page, size);
    return this.webService.get(SERVER.DOCTORS + '/search' + response, this.webService.JSONWhitHeaders());
  }

  uploadImage(file?: any, userId?: any) {
    const formData: FormData = new FormData();
    formData.append('userId', userId);
    formData.append('file', file);
    const headers = this.webService.onlyTokenHeader(this.authService.getToken());
    return this.webService.post(SERVER.DOCTORS + '/update-current-avatar', formData, headers);
  }

}
