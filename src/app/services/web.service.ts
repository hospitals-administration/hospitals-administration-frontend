import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebService {

  constructor(
    private http: HttpClient
  ) { }

  get(url: any, options?: any): Observable<any> {
    options = options ? options : this.defaultOptions();
    return this.http.get<any>(url, options);
  }

  post(url: any, body: any, options?: any): Observable<any> {
    options = options ? options : this.defaultOptions();
    return this.http.post<any>(url, body, options);
  }

  put(url: any, body: any, options?: any): Observable<any> {
    options = options ? options : this.defaultOptions();
    return this.http.put<any>(url, body, options);
  }

  delete(url: any, options?: any): Observable<any> {
    options = options ? options : this.defaultOptions();
    return this.http.delete<any>(url, options);
  }

  textOptions(token?: any) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    if (token) {
      headers = headers.append('Authorization', 'Bearer ' + token);
    }
    return { headers: headers, responseType: 'text' };
  }

  JSONOptions(token?: any) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    if (token) {
      headers = headers.append('Authorization', 'Bearer ' + token);
    }
    return { headers: headers };
  }

  defaultOptions() {
    return this.JSONOptions();
  }

  JSONWhitHeaders() {
    const options = {
      headers: this.defaultOptions,
      observe: 'response' as 'body',
      responseType: 'json'
    };
    return options;
  }

  pageSize(page?: any, size?: any): string {
    return '?page=' + page + '&size=' + size;
  }

  onlyTokenHeader(token?: any) {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Bearer ' + token);
    return { headers: headers, responseType: 'text' };
  }

  imageFileHeaders(token?: any) {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Bearer ' + token);
    return { headers: headers, responseType: 'blob' };
  }

}
