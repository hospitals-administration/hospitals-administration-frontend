import { Injectable } from '@angular/core';
// SERVER CONFIG
import SERVER from '../config/server.config';
import { AuthService } from './auth.service';
// SERVICES
import { WebService } from './web.service';

@Injectable({
  providedIn: 'root'
})
export class HospitalService {

  public options: any;

  constructor(
    private webService: WebService,
    private authService: AuthService
  ) {
    this.options = this.webService.JSONOptions(this.authService.getToken());
   }

  findAll(page?: any, size?: any) {
    return this.webService.get(SERVER.HOSPITALS + this.webService.pageSize(page, size), this.webService.JSONWhitHeaders());
  }

  create(hospital: any) {
    return this.webService.post(SERVER.HOSPITALS, hospital, this.options);
  }

  update(hospital: any) {
    return this.webService.put(SERVER.HOSPITALS, hospital, this.options);
  }

  findOne(hospitalId: any) {
    return this.webService.get(SERVER.HOSPITALS + '/' + hospitalId, this.options);
  }

  deleteOne(hospitalId: any) {
    return this.webService.delete(SERVER.HOSPITALS + '/' + hospitalId, this.options);
  }

  // SEARCH
  search(name?: any, date?: any, page?: any, size?: any) {
    let response = null;
    if (name !== undefined && date === undefined) {
      response = '?name=' + name;
    }
    if (name === undefined && date !== undefined) {
      response = '?createdate=' + date;
    }
    if (name !== undefined && date !== undefined) {
      response = '?name=' + name + '&createdate=' + date;
    }
    response = response !== null ? response + this.webService.pageSize(page, size).replace('?', '&') : response === null? '' : response + this.webService.pageSize(page, size);
    console.log(response);
    return this.webService.get(SERVER.HOSPITALS + '/search' + response, this.webService.JSONWhitHeaders());
  }
  
}
