import { Injectable } from '@angular/core';
import SERVER from '../config/server.config';
import { AuthService } from './auth.service';
import { WebService } from './web.service';

@Injectable({
  providedIn: 'root'
})
export class VisitNoteService {
  
  public options: any;

  constructor(
    private webService: WebService,
    private authService: AuthService
  ) { 
    this.options = this.webService.JSONOptions(this.authService.getToken());
  }

  findAllByPatientId(id?: any, page?: any, size?: any) {
    return this.webService.get(SERVER.VISITNOTES + '/patient/' + id + this.webService.pageSize(page, size), this.webService.JSONWhitHeaders());
  }

  findAll(page?: any, size?: any) {
    return this.webService.get(SERVER.VISITNOTES + this.webService.pageSize(page, size), this.webService.JSONWhitHeaders());
  }

  create(speciality: any) {
    return this.webService.post(SERVER.VISITNOTES, speciality, this.options);
  }

  update(speciality: any) {
    return this.webService.put(SERVER.VISITNOTES, speciality, this.options);
  }

  findOne(specialityId: any) {
    return this.webService.get(SERVER.VISITNOTES + '/' + specialityId, this.options);
  }

  deleteOne(specialityId: any) {
    return this.webService.delete(SERVER.VISITNOTES + '/' + specialityId, this.options);
  }

}
