import { Injectable } from '@angular/core';
// SERVER CONFIG
import SERVER from '../config/server.config';
import { AuthService } from './auth.service';
// SERVICES
import { WebService } from './web.service';

@Injectable({
  providedIn: 'root'
})
export class SpecialityService {

  public options: any;

  constructor(
    private webService: WebService,
    private authService: AuthService
  ) { 
    this.options = this.webService.JSONOptions(this.authService.getToken());
  }

  findAll(page?: any, size?: any) {
    return this.webService.get(SERVER.SPECIALITIES + this.webService.pageSize(page, size), this.webService.JSONWhitHeaders());
  }

  create(speciality: any) {
    return this.webService.post(SERVER.SPECIALITIES, speciality, this.options);
  }

  update(speciality: any) {
    return this.webService.put(SERVER.SPECIALITIES, speciality, this.options);
  }

  findOne(specialityId: any) {
    return this.webService.get(SERVER.SPECIALITIES + '/' + specialityId, this.options);
  }

  deleteOne(specialityId: any) {
    return this.webService.delete(SERVER.SPECIALITIES + '/' + specialityId, this.options);
  }

  // IMAGE

  uploadImage(file?: any, userId?: any) {
    const formData: FormData = new FormData();
    formData.append('userId', userId);
    formData.append('file', file);
    const headers = this.webService.onlyTokenHeader(this.authService.getToken());
    return this.webService.post(SERVER.SPECIALITIES + '/update-current-avatar', formData, headers);
  }

}
