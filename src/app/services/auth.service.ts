import { Injectable } from '@angular/core';
// SERVER CONFIG
import SERVER from '../config/server.config';
// SERVICES
import { WebService } from './web.service';
// JWT
import jwt_decode from "jwt-decode";
// RXJS
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private webService: WebService,
    private router: Router
  ) { }

  login(user: any) {
    const options = this.webService.defaultOptions();
    return this.webService.post(SERVER.AUTHENTICATE, user, options);
  }

  logout() {
    localStorage.removeItem('hospital-token');
    sessionStorage.removeItem('hospital-token');
  }

  getToken() {
    return sessionStorage.getItem('hospital-token') ? sessionStorage.getItem('hospital-token') : localStorage.getItem('hospital-token') ? localStorage.getItem('hospital-token') : null;
  }

  getAccount() {
    return this.webService.get(SERVER.ACCOUNT, this.webService.JSONOptions(this.getToken()));
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = Object.assign({}, jwt_decode(token));
    const data = JSON.stringify(decoded);
    const data1 = [JSON.parse(data)];
    const obj =  data1[0].exp;
    if (obj === undefined) {
      return new Date(0);
    }
    const date = new Date(0);
    date.setUTCSeconds(Number(obj));
    return date;
  }

  isTokenExpired(token?: any): boolean {
    if (!token) {
      token = this.getToken();
    }
    if (!token) {
      return true;
    }
    const date = this.getTokenExpirationDate(token);
    if (date === undefined) {
      return false;
    }
    const expired = !(date.valueOf() > new Date().valueOf());
    if (expired) {
      this.logout();
    }
    return expired;
  }

  isLogged() {
    return this.webService.get(SERVER.AUTHENTICATE, this.webService.textOptions(this.getToken())).pipe(map(res => {
      const logged = res && !this.isTokenExpired();
      if (logged) {
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      }
    }));
  }

}
