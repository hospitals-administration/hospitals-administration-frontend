import { environment } from "src/environments/environment";

const SERVER = (() => {
    const IMAGE = environment.image;
    const BASE = {
        URL: environment.baseUrl
    };
    const AUTHENTICATE = 'authenticate';
    const ACCOUNT = 'account';
    const USERS = 'users';
    const ROLES = 'roles'
    const HOSPITALS = 'hospitals';
    const SPECIALITIES = 'specialities';
    const DOCTORS = 'doctors';
    const PATIENTS = 'patients';
    const VISITNOTES = 'visit-notes';
    return {
        IMAGE: IMAGE,
        BASE_URL: BASE.URL,
        AUTHENTICATE: `${BASE.URL}/${AUTHENTICATE}`,
        ACCOUNT: `${BASE.URL}/${ACCOUNT}`,
        USERS: `${BASE.URL}/${USERS}`,
        ROLES: `${BASE.URL}/${ROLES}`,
        HOSPITALS: `${BASE.URL}/${HOSPITALS}`,
        SPECIALITIES: `${BASE.URL}/${SPECIALITIES}`,
        DOCTORS: `${BASE.URL}/${DOCTORS}`,
        PATIENTS: `${BASE.URL}/${PATIENTS}`,
        VISITNOTES: `${BASE.URL}/${VISITNOTES}`
    };
})();

export default SERVER;