import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import SERVER from 'src/app/config/server.config';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  public user: any;
  public imageUrl: any;
  public responsive: any = 'close-responsive';

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private _route: Router
  ) { 
    this.currentUser();
  }

  ngOnInit(): void {
  }

  currentUser() {
    this.authService.getAccount().subscribe(
      (response) => {
        this.user = response;
        if (this.user.imageUrl) {
          this.imageUrl = SERVER.IMAGE + this.user.imageUrl;
        } else {
          this.imageUrl = "http://angular-material.fusetheme.com/assets/images/avatars/Velazquez.jpg";
        }
      }, (error) => {
        console.log(error);
      }
    )
  }

  logout() {
    console.log('-=========== SALIR -------------');
    this.authService.logout();
    this._route.navigate(['/login']);
  }

  toggleResponsive() {
    if (this.responsive === 'open-responsive') {
      this.responsive = 'close-responsive';
    } else {
      this.responsive = 'open-responsive';
    }
  }

}
