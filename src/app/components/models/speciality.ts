export class Speciality {
	constructor(
		public name: string,
		public description: string,
		public avatar: string
	) { }
}