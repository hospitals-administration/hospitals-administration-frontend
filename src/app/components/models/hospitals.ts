export class Hospital {
	constructor(
		public name: string,
		public address: string,
		public phone: string,
		public activated: boolean
	) { }
}