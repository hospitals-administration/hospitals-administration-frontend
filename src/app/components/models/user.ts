export class User {
  constructor(
    public id: number,
    public firstName: string,
    public lastName: string,
    public address: string,
    public birthDate: any,
    public profilePhoto: string
  ) { }
}