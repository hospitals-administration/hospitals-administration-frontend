import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlertRoutingModule } from './alert-routing.module';
import { AlertComponent } from './alert/alert.component';

import { AlertModule as AlertNgxBoostrap } from 'ngx-bootstrap/alert';

@NgModule({
  declarations: [AlertComponent],
  imports: [
    CommonModule,
    AlertRoutingModule,
    AlertNgxBoostrap.forRoot()
  ],
  exports: [AlertComponent]
})
export class AlertModule { }
