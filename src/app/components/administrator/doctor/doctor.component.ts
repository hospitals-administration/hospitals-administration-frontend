import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { DatePipe } from '@angular/common';
import {LOCALE_ID} from '@angular/core';
@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.scss'],
  providers: [ DatePipe ]
})
export class DoctorComponent implements OnInit {
  
  public doctors: any = [];
  public findAllSubscription!: Subscription;
  public doctorTest: any = null;
  public hospitalId = null;
  // PAGINATION
  public pageSize = 5;
  public totalItems = 0;
  public currentPage = 1;
  public rotate = true;
  public maxSize = 5;
  // SEARCH
  public name: any;
  public date: any;

  public user: any;
  public authorities: any = [];
  
  constructor(
    private doctorService: DoctorService,
    private alertService: AlertService,
    private authService: AuthService
  ) {
    this.currentUser();
  }

  ngOnInit(): void {
    this.findAll();
  }
  
  currentUser() {
    this.authService.getAccount().subscribe(
      (response) => {
        this.user = response;
        this.authorities = response.authorities;
      }, (error) => {
        console.log(error);
      }
    )
  }

  findAll() {
    this.findAllSubscription = this.doctorService.findAll(this.currentPage - 1, this.pageSize).subscribe(
      (response) => {
        this.doctors = response.body;
        this.totalItems = response.headers.get('X-Total-Count');
      }, (error) => {
        console.log(error);
      }
    );
  }

  delete(hospitalId: number) {
    this.doctorService.deleteOne(hospitalId).subscribe(
      (response) => {
        this.findAll();
        this.alertService.success('The doctor was deleted successfully');
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    );
  }

  changePage(event: any): void {
    this.currentPage = event.page;
    this.findAll();
  }

  setPage(pageNo: any): void {
    this.currentPage = pageNo;
  }

  ngOnDestroy(): void {
    this.findAllSubscription.unsubscribe();
  }

  // SEARCH
  search() {
    this.doctorService.search(this.name, this.date, this.currentPage - 1, this.pageSize).subscribe(
      (response) => {
        this.doctors = response.body;
        this.totalItems = response.headers.get('X-Total-Count');
        const data = response.body.length === 0 ? 'No Results' : response.body.length + ' doctors';
        this.alertService.success('Successful search : ' + data);
      }, (error) => {
        console.log(error);
      }
    );
  }

  clear() {
    this.name = undefined;
    this.date = undefined;
    this.findAll()
  }

  tranform(today?: any) {
    console.log(LOCALE_ID);
    return new DatePipe('en-Us').transform(today, 'dd:MM:yyyy hh-mm-ss', 'GMT+1');
  }

}
