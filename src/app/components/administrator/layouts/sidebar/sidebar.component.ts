import { Component, Input, OnInit } from '@angular/core';
import SERVER from 'src/app/config/server.config';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public user: any;
  public authorities: any = [];
  public imageUrl: any;
  @Input() responsive: any = '';

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.currentUser();
  }

  currentUser() {
    this.authService.getAccount().subscribe(
      (response) => {
        this.user = response;
        if (this.user.imageUrl) {
          this.imageUrl = SERVER.IMAGE + this.user.imageUrl;
        } else {
          this.imageUrl = "http://angular-material.fusetheme.com/assets/images/avatars/Velazquez.jpg";
        }
        this.authorities = response.authorities;
      }, (error) => {
        console.log(error);
      }
    )
  }

}
