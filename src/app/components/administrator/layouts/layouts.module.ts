import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutsRoutingModule } from './layouts-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FotterComponent } from './fotter/fotter.component';


@NgModule({
  declarations: [
    NavbarComponent,
    SidebarComponent,
    FotterComponent
  ],
  imports: [
    CommonModule,
    LayoutsRoutingModule
  ],
  exports: [
    NavbarComponent,
    SidebarComponent,
    FotterComponent
  ]
})
export class LayoutsModule { }
