import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IMultiSelectSettings, IMultiSelectTexts } from 'ngx-bootstrap-multiselect';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { Observable, zip } from 'rxjs';
import SERVER from 'src/app/config/server.config';
import { AlertService } from 'src/app/services/alert.service';
import { DoctorService } from 'src/app/services/doctor.service';
import { HospitalService } from 'src/app/services/hospital.service';
import { SpecialityService } from 'src/app/services/speciality.service';

@Component({
  selector: 'app-doctor-modal',
  templateUrl: './doctor-modal.component.html',
  styleUrls: ['./doctor-modal.component.scss']
})
export class DoctorModalComponent implements OnInit {

  public doctorFormGroup!: FormGroup;
  public modalRef!: BsModalRef;
  public submitted: any = false;
  @Output() findAll: EventEmitter<null> = new EventEmitter();
  @Input() doctor = {};
  @Input() hospitalId = null;
  public hospitals = [];
  public specialities = [];
  public speciality = {};

  // Specialities
  public selectActive = 'select-one';
  // Settings configuration
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true
  };

  // Text configuration
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'specialities selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select',
    allSelected: 'All selected',
  };

  // FILE
  public fileName: any;
  public localImageUrl: any = '';

  constructor(
    private doctorService: DoctorService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private hospitalService: HospitalService,
    private specialityService: SpecialityService
  ) { }

  ngOnInit(): void {
    // this.findAllHospitals();
    this.validateFormGroup();
  }

  validateFormGroup() {
    this.doctorFormGroup = this.formBuilder.group({
      id: [null],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      birthDate: ['', [Validators.required]],
      address: [''],
      hospital: [this.hospitalId],
      hospitalSelect: [''],
      authorities: ['ROLE_DOCTOR'],
      speciality: [{}],
      specialities: [],
      hospitalDoctorId: null,
      login: ['', Validators.required],
      email: null,
      profilePhoto: null,
      imageUrl: null,
      createdBy: [''],
      createdDate: [null],
      lastModifiedBy: [''],
      lastModifiedDate: [null]
    });
  }

  getHospitalOfUser() {
    const data1 = this.stringify(this.doctor);
    const data2 = [JSON.parse(data1)];
    this.doctorFormGroup.patchValue({
      hospitalSelect: data2[0] !== null ? this.stringify(data2[0].hospitalDTO) : 0,
      speciality: data2[0] !== null ? data2[0].specialities.map((x: { id: any; }) => x.id) : []
    });
  }

  get formValue() {
    return this.doctorFormGroup.value;
  }

  get formControls() {
    return this.doctorFormGroup.controls;
  }

  onSubmit() {
    console.log('-----------------------');
    this.doctorFormGroup.patchValue({
      hospital: [JSON.parse(this.formValue.hospitalSelect)][0],
      hospitalDoctorId: this.hospitalId !== null ? Number(this.hospitalId) : [JSON.parse(this.formValue.hospitalSelect)][0].id
    });
    console.log(this.formValue);
    console.log('-----------------------');
    this.submitted = true;
    if (this.doctorFormGroup.invalid) {
      return;
    }
    if (this.formValue.id) {
      this.update();
    } else {
      this.create();
    }
  }

  create() {
    this.doctorService.create(this.formValue).subscribe(
      (response) => {
        console.log(response);
        this.modalRef.hide();
        this.uploadFile(response.id);
        this.click();
        this.alertService.success('The doctor was created successfully');
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    )
  }

  update() {
    this.doctorService.update(this.formValue).subscribe(
      (response) => {
        console.log(response);
        this.modalRef.hide();
        this.uploadFile(response.id);
        this.alertService.success('The doctor was updated successfully');
        // this.refresh();
        this.click();
        setTimeout(() => {
          this.refresh()
        }, 1000);
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    )
  }

  click() {
    this.findAll.emit();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.submitted = false;
    this.doctorFormGroup.reset();
    this.getHospitalOfUser();
    this.findAllHospitals();
    this.findAllSpecialities();
    if (this.doctor !== null) {
      this.doctorFormGroup.patchValue(this.doctor);
      this.localImageUrl = SERVER.IMAGE + this.formValue.imageUrl;
    }
  }

  findAllHospitals() {
    this.hospitalService.findAll(0, 1000).subscribe(
      (response) => {
        this.hospitals = response.body;
      }, (error) => {
        console.log(error);
      }
    );
  }

  findAllSpecialities() {
    this.specialityService.findAll(0, 200).subscribe(
      (response) => {
        this.specialities = response.body;
      }, (error) => {
        console.log(error);
      }
    )
  }

  stringify(o: any): string {
    return JSON.stringify(o);
  };

  converterObject(o: any) {
    const data = JSON.stringify(o);
    const dep = [JSON.parse(data)]
    return dep[0].name;
  }

  addedLugarExpedido(departamento: any, value?: any) {
    const dep = [JSON.parse(departamento.value)];
    this.doctorFormGroup.patchValue({
      hospital: dep[0].name
    });
  }

  converterSpeciality(o: any) {
    const data = JSON.stringify(o);
    const dep = [JSON.parse(data)]
    return dep[0];
  }

  onChange(event: any) {
    const data1 = this.specialities.filter((x: any) => {
      return event.includes(x.id);
    })
    this.doctorFormGroup.patchValue({
      specialities: data1
    });
  }

  // ADDED IMAGE
  uploadFile(patientId?: any) {
    if (this.fileName) {
      this.doctorService.uploadImage(this.fileName, patientId).subscribe(
        (res) => {
          this.formValue.imageUrl = res;
          this.alertService.success('successfully');
        },
        (error) => {
          this.alertService.error(error.error.title);
        }
      );
    }
  }

  uploadImage(fileImage?: any) {
    if (fileImage.target.files) {
      const file = fileImage.target.files.item(0);
      if (file.type.match('image.*')) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = () => {
          this.localImageUrl = reader.result;
        };
        this.fileName = file;
        console.log('el archo ----------->>> ', this.fileName);
      } else {
        console.log('invalid format!');
      }
    }
  }

  refresh(): void {
    window.location.reload();
  }

}
