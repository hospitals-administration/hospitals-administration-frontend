import { Component, EventEmitter, OnInit, Output, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SpecialityService } from 'src/app/services/speciality.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit {

  public modalRef!: BsModalRef;
  @Output() delete: EventEmitter<null> = new EventEmitter();

  constructor(
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirm(): void {
    this.click();
    this.modalRef.hide();
  }

  click() {
    this.delete.emit();
  }

  decline(): void {
    this.modalRef.hide();
  }

}
