import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitNoteModalComponent } from './visit-note-modal.component';

describe('VisitNoteModalComponent', () => {
  let component: VisitNoteModalComponent;
  let fixture: ComponentFixture<VisitNoteModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisitNoteModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitNoteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
