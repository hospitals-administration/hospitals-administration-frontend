import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { VisitNoteService } from 'src/app/services/visit-note.service';

@Component({
  selector: 'app-visit-note-modal',
  templateUrl: './visit-note-modal.component.html',
  styleUrls: ['./visit-note-modal.component.scss'],
  providers: [ DatePipe ]
})
export class VisitNoteModalComponent implements OnInit {

  public visitNoteFormGroup!: FormGroup;
  public modalRef!: BsModalRef;
  public submitted: any = false;
  @Output() findAll: EventEmitter<null> = new EventEmitter();
  @Input() visitNote: any = {};
  private doctor: any = {};
  
  constructor(
    private visitNoteService: VisitNoteService,
    private authService: AuthService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private _route: Router,
    private route: ActivatedRoute,
    private datePipe: DatePipe
  ) {
    this.currentUser();
  }

  ngOnInit(): void {
    this.validateFormGroup();
  }

  currentUser() {
    this.authService.getAccount().subscribe(
      (response) => {
        this.doctor = response;
      }, (error) => {
        console.log(error);
      }
    )
  }

  validateFormGroup() {
    this.visitNoteFormGroup = this.formBuilder.group({
      id: [null],
      visitDate: [Date.now, [Validators.required]],
      description: ['', [Validators.required]],
      userDoctorId: [null],
      userPatientId: [null],
      hospitalId: [null],
      createdBy: [''],
      createdDate: [null],
      lastModifiedBy: [''],
      lastModifiedDate: [null]
    });
  }

  get formValue() {
    return this.visitNoteFormGroup.value;
  }

  get formControls() {
    return this.visitNoteFormGroup.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.visitNoteFormGroup.invalid) {
      return;
    }
    if (this.formValue.id) {
      this.update();
    } else {
      this.visitNoteFormGroup.patchValue({ 
        userDoctorId: this.doctor?.id,
        userPatientId: Number(this.route.snapshot.params.id),
        hospitalId: this.doctor?.hospitalDTO?.id,
      });
      console.log('----------------------------');
      console.log(this.formValue);
      console.log('----------------------------');
      this.create();
    }
  }

  create() {
    this.visitNoteService.create(this.formValue).subscribe(
      (response) => {
        console.log(response);
        this.modalRef.hide();
        this.click();
        this.alertService.success('The vist note was created successfully');
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    )
  }

  update() {
    this.visitNoteService.update(this.formValue).subscribe(
      (response) => {
        console.log(response);
        this.modalRef.hide();
        this.click();
        this.alertService.success('The vist note was updated successfully');
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    )
  }

  click() {
    this.findAll.emit();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.submitted = false;
    this.visitNoteFormGroup.reset();
    const today = new Date;
    this.visitNoteFormGroup.patchValue({ visitDate: this.datePipe.transform(today, 'yyyy-MM-dd')});
    if (this.visitNote !== null) {
      this.visitNoteFormGroup.patchValue(this.visitNote);
    }
  }

}
