import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IMultiSelectSettings, IMultiSelectTexts } from 'ngx-bootstrap-multiselect';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import SERVER from 'src/app/config/server.config';
import { AlertService } from 'src/app/services/alert.service';
import { HospitalService } from 'src/app/services/hospital.service';
import { PatientService } from 'src/app/services/patient.service';

@Component({
  selector: 'app-patient-modal',
  templateUrl: './patient-modal.component.html',
  styleUrls: ['./patient-modal.component.scss']
})
export class PatientModalComponent implements OnInit {

  public patientFormGroup!: FormGroup;
  public modalRef!: BsModalRef;
  public submitted: any = false;
  @Output() findAll: EventEmitter<null> = new EventEmitter();
  @Input() patient = {};
  @Input() hospitalId = null;
  public hospitals = [];
  public specialities = [];
  public speciality = {};

  // Specialities
  public selectActive = 'select-one';
  // Settings configuration
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true
  };

  // Text configuration
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'specialities selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select',
    allSelected: 'All selected',
  };

  // FILE
  public fileName: any;
  public localImageUrl: any = '';

  constructor(
    private patientService: PatientService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private hospitalService: HospitalService,
    private _route: Router
  ) { }

  ngOnInit(): void {
    this.validateFormGroup();
  }

  validateFormGroup() {
    this.patientFormGroup = this.formBuilder.group({
      id: [null],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      birthDate: ['', [Validators.required]],
      address: [''],
      hospital: [this.hospitalId],
      hospitalSelect: [''],
      authorities: ['ROLE_PATIENT'],
      speciality: [{}],
      specialities: [],
      hospitalDoctorId: null,
      hospitalPatientId: null,
      login: ['', Validators.required],
      email: null,
      profilePhoto: null,
      imageUrl: null,
      createdBy: [''],
      createdDate: [null],
      lastModifiedBy: [''],
      lastModifiedDate: [null]
    });
  }

  getHospitalOfUser() {
    const data1 = this.stringify(this.patient);
    const data2 = [JSON.parse(data1)];
    this.patientFormGroup.patchValue({
      hospitalSelect: data2[0] !== null ? this.stringify(data2[0].hospitalDTO) : 0,
      speciality: data2[0] !== null ? data2[0].specialities.map((x: { id: any; }) => x.id) : [],
    });
  }

  get formValue() {
    return this.patientFormGroup.value;
  }

  get formControls() {
    return this.patientFormGroup.controls;
  }

  onSubmit() {
    this.patientFormGroup.patchValue({
      hospital: [JSON.parse(this.formValue.hospitalSelect)][0],
      hospitalPatientId: this.hospitalId !== null ? Number(this.hospitalId) : [JSON.parse(this.formValue.hospitalSelect)][0].id,
      hospitalDoctorId: null,
    });
    this.submitted = true;
    if (this.patientFormGroup.invalid) {
      return;
    }
    if (this.formValue.id) {
      this.update();
    } else {
      this.create();
    }
  }

  create() {
    this.patientService.create(this.formValue).subscribe(
      (response) => {
        console.log(response);
        this.modalRef.hide();
        this.uploadFile(response.id);
        this.click();
        this.alertService.success('The patient was created successfully');
        this._route.navigate(['/admin/visit-notes/patient/' + response.id]);
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    )
  }

  update() {
    this.patientService.update(this.formValue).subscribe(
      (response) => {
        console.log(response);
        this.modalRef.hide();
        this.uploadFile(response.id);
        this.localImageUrl = null;
        this.alertService.success('The patient was updated successfully');
        this.patient = Object.assign({}, {});
        // this.refresh();
        this.click();
        setTimeout(() => {
          this.refresh()
        }, 1000);
        // this._route.navigate(['/admin/visit-notes/patient/' + response.id]);
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    )
  }

  click() {
    this.findAll.emit();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.submitted = false;
    this.patientFormGroup.reset();
    this.getHospitalOfUser();
    this.findAllHospitals();
    if (this.patient !== null) {
      this.patientFormGroup.patchValue(this.patient);
      this.localImageUrl = SERVER.IMAGE + this.formValue.imageUrl;
    }
  }

  findAllHospitals() {
    this.hospitalService.findAll(0, 1000).subscribe(
      (response) => {
        this.hospitals = response.body;
      }, (error) => {
        console.log(error);
      }
    );
  }

  stringify(o: any): string {
    return JSON.stringify(o);
  };

  converterObject(o: any) {
    const data = JSON.stringify(o);
    const dep = [JSON.parse(data)]
    return dep[0].name;
  }

  addedLugarExpedido(departamento: any, value?: any) {
    const dep = [JSON.parse(departamento.value)];
    this.patientFormGroup.patchValue({
      hospital: dep[0].name
    });
  }

  // ADDED IMAGE
  uploadFile(patientId?: any) {
    if (this.fileName) {
      this.patientService.uploadImage(this.fileName, patientId).subscribe(
        (res) => {
          this.formValue.imageUrl = res;
          this.alertService.success('successfully');
        },
        (error) => {
          this.alertService.error(error.error.title);
        }
      );
    }
  }

  uploadImage(fileImage?: any) {
    if (fileImage.target.files) {
      const file = fileImage.target.files.item(0);
      if (file.type.match('image.*')) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = () => {
          this.localImageUrl = reader.result;
        };
        this.fileName = file;
        console.log('el archo ----------->>> ', this.fileName);
        // this.patientFormGroup.patchValue({
        //   imageUrl: '-'
        // });
      } else {
        console.log('invalid format!');
      }
    }
  }

  refresh(): void {
    window.location.reload();
  }

}
