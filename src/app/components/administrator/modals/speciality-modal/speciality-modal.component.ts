import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import SERVER from 'src/app/config/server.config';
import { AlertService } from 'src/app/services/alert.service';
import { SpecialityService } from 'src/app/services/speciality.service';

@Component({
  selector: 'app-speciality-modal',
  templateUrl: './speciality-modal.component.html',
  styleUrls: ['./speciality-modal.component.scss']
})
export class SpecialityModalComponent implements OnInit {

  public specialityFormGroup!: FormGroup;
  public modalRef!: BsModalRef;
  public submitted: any = false;
  @Output() findAll: EventEmitter<null> = new EventEmitter();
  @Input() speciality = {};

  // FILE
  public fileName: any;
  public localImageUrl: any = '';

  constructor(
    private specialityService: SpecialityService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) {
  }

  ngOnInit(): void {
    this.validateFormGroup();
  }

  validateFormGroup() {
    this.specialityFormGroup = this.formBuilder.group({
      id: [null],
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      avatar: null,
      createdBy: [''],
      createdDate: [null],
      lastModifiedBy: [''],
      lastModifiedDate: [null]
    });
  }

  get formValue() {
    return this.specialityFormGroup.value;
  }

  get formControls() {
    return this.specialityFormGroup.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.specialityFormGroup.invalid) {
      return;
    }
    if (this.formValue.id) {
      this.update();
    } else {
      this.create();
    }
  }

  create() {
    this.specialityService.create(this.formValue).subscribe(
      (response) => {
        console.log(response);
        this.modalRef.hide();
        this.uploadFile(response.id);
        this.click();
        this.alertService.success('The specility was created successfully');
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    )
  }

  update() {
    this.specialityService.update(this.formValue).subscribe(
      (response) => {
        this.modalRef.hide();
        this.specialityFormGroup.patchValue({
          avatar: response.avatar
        });
        this.uploadFile(response.id);
        this.click();
        this.alertService.success('The specility was updated successfully');
        setTimeout(() => {
          this.refresh()
        }, 1000);
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    )
  }

  click() {
    this.findAll.emit();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.submitted = false;
    this.specialityFormGroup.reset();
    if (this.speciality !== null) {
      this.specialityFormGroup.patchValue(this.speciality);
      this.localImageUrl = SERVER.IMAGE + this.formValue.avatar;
    }
  }

  // ADDED IMAGE
  uploadFile(patientId?: any) {
    if (this.fileName) {
      this.specialityService.uploadImage(this.fileName, patientId).subscribe(
        (res) => {
          this.formValue.imageUrl = res;
          this.alertService.success('successfully');
          this.click();
        },
        (error) => {
          this.alertService.error(error.error.title);
        }
      );
    }
  }

  uploadImage(fileImage?: any) {
    if (fileImage.target.files) {
      const file = fileImage.target.files.item(0);
      if (file.type.match('image.*')) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = () => {
          this.localImageUrl = reader.result;
        };
        this.fileName = file;
        console.log('el archo ----------->>> ', this.fileName);
        // this.specialityFormGroup.patchValue({
        //   imageUrl: '-'
        // });
      } else {
        console.log('invalid format!');
      }
    }
  }
  
  refresh(): void {
    window.location.reload();
  }

}
