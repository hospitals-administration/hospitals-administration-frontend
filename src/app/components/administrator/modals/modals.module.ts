import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalsRoutingModule } from './modals-routing.module';
import { SpecialityModalComponent } from './speciality-modal/speciality-modal.component';
import { HospitalModalComponent } from './hospital-modal/hospital-modal.component';
import { DoctorModalComponent } from './doctor-modal/doctor-modal.component';
import { PatientModalComponent } from './patient-modal/patient-modal.component';
import { UserModalComponent } from './user-modal/user-modal.component';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NgxBootstrapMultiselectModule } from 'ngx-bootstrap-multiselect';
import { VisitNoteModalComponent } from './visit-note-modal/visit-note-modal.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
@NgModule({
  declarations: [
    SpecialityModalComponent,
    HospitalModalComponent,
    DoctorModalComponent,
    PatientModalComponent,
    UserModalComponent,
    DeleteModalComponent,
    VisitNoteModalComponent
  ],
  imports: [
    CommonModule,
    ModalsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    NgxBootstrapMultiselectModule,
    TooltipModule.forRoot(),
  ],
  exports: [
    CommonModule,
    ModalsRoutingModule,
    SpecialityModalComponent,
    HospitalModalComponent,
    DoctorModalComponent,
    PatientModalComponent,
    UserModalComponent,
    DeleteModalComponent,
    VisitNoteModalComponent
  ]
})
export class ModalsModule { }
