import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AlertService } from 'src/app/services/alert.service';
import { HospitalService } from 'src/app/services/hospital.service';

@Component({
  selector: 'app-hospital-modal',
  templateUrl: './hospital-modal.component.html',
  styleUrls: ['./hospital-modal.component.scss']
})
export class HospitalModalComponent implements OnInit {

  public hospitalFormGroup!: FormGroup;
  public modalRef!: BsModalRef;
  public submitted: any = false;
  @Output() findAll: EventEmitter<null> = new EventEmitter();
  @Input() hospital = {};

  constructor(
    private hospitalService: HospitalService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.validateFormGroup();
  }

  validateFormGroup() {
    this.hospitalFormGroup = this.formBuilder.group({
      id: [null],
      name: ['', [Validators.required]],
      address: ['', [Validators.required]],
      createdBy: [''],
      createdDate: [null],
      lastModifiedBy: [''],
      lastModifiedDate: [null]
    });
  }

  get formValue() {
    return this.hospitalFormGroup.value;
  }

  get formControls() {
    return this.hospitalFormGroup.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.hospitalFormGroup.invalid) {
      return;
    }
    if (this.formValue.id) {
      this.update();
    } else {
      this.create();
    }
  }

  create() {
    this.hospitalService.create(this.formValue).subscribe(
      (response) => {
        console.log(response);
        this.modalRef.hide();
        this.click();
        this.alertService.success('The hospital was created successfully');
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    )
  }

  update() {
    this.hospitalService.update(this.formValue).subscribe(
      (response) => {
        console.log(response);
        this.modalRef.hide();
        this.click();
        this.alertService.success('The specility was updated successfully');
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    )
  }

  click() {
    this.findAll.emit();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.submitted = false;
    this.hospitalFormGroup.reset();
    if (this.hospital !== null) {
      this.hospitalFormGroup.patchValue(this.hospital);
    }
  }

}
