import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { SpecialityService } from 'src/app/services/speciality.service';

@Component({
  selector: 'app-speciality',
  templateUrl: './speciality.component.html',
  styleUrls: ['./speciality.component.scss']
})
export class SpecialityComponent implements OnInit {

  public specialities: any = [];
  public specialityTest: any = null;
  showBoundaryLinks = true;
  // PAGINATION
  public pageSize = 5;
  public totalItems = 0;
  public currentPage = 1;
  public rotate = true;
  public maxSize = 5;

  public user: any;
  public authorities: any = [];

  constructor(
    private specialtyService: SpecialityService,
    private alertService: AlertService,
    private authService: AuthService
  ) {
    this.currentUser();
   }

  ngOnInit(): void {
    this.findAll();
  }

  currentUser() {
    this.authService.getAccount().subscribe(
      (response) => {
        this.user = response;
        this.authorities = response.authorities;
      }, (error) => {
        console.log(error);
      }
    )
  }


  findAll() {
    this.specialtyService.findAll(this.currentPage - 1, this.pageSize).subscribe(
      (response) => {
        this.specialities = response.body;
        this.totalItems = response.headers.get('X-Total-Count');
      }, (error) => {
        console.log(error);
      }
    )
  }

  delete(specialityId: number) {
    this.specialtyService.deleteOne(specialityId).subscribe(
      (response) => {
        console.log(response);
        this.findAll();
        this.alertService.success('The vist note was deleted successfully');
      }, (error) => {
        console.log(error);
        this.alertService.error(error.error.title);
      }
    );
  }

  changePage(event: any): void {
    this.currentPage = event.page;
    this.findAll();
  }

  setPage(pageNo: any): void {
    this.currentPage = pageNo;
  }

}
