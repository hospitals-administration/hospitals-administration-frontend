import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';
import { PatientService } from 'src/app/services/patient.service';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit, OnDestroy {

  public patients: any = [];
  public findAllSubscription!: Subscription;
  public patientTest: any = null;
  public hospitalId = null;
  // PAGINATION
  public pageSize = 5;
  public totalItems = 0;
  public currentPage = 1;
  public rotate = true;
  public maxSize = 5;
  // SEARCH
  public name: any;
  public date: any;

  constructor(
    private patientService: PatientService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.findAll();
  }

  findAll() {
    this.findAllSubscription = this.patientService.findAll(this.currentPage - 1, this.pageSize).subscribe(
      (response) => {
        this.patients = response.body;
        this.totalItems = response.headers.get('X-Total-Count');
      }, (error) => {
        console.log(error);
      }
    );
  }

  delete(hospitalId: number) {
    this.patientService.deleteOne(hospitalId).subscribe(
      (response) => {
        this.findAll();
        this.alertService.success('The patient was deleted successfully');
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    );
  }

  changePage(event: any): void {
    this.currentPage = event.page;
    this.findAll();
  }

  setPage(pageNo: any): void {
    this.currentPage = pageNo;
  }

  ngOnDestroy(): void {
    this.findAllSubscription.unsubscribe();
  }

  // SEARCH
  search() {
    this.patientService.search(this.name, this.date, this.currentPage - 1, this.pageSize).subscribe(
      (response) => {
        this.patients = response.body;
        this.totalItems = response.headers.get('X-Total-Count');
        const data = response.body.length === 0 ? 'No Results' : response.body.length + ' patients';
        this.alertService.success('Successful search : ' + data);
      }, (error) => {
        console.log(error);
      }
    );
  }
  
  clear() {
    this.name = undefined;
    this.date = undefined;
    this.findAll()
  }

}
