import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { PatientService } from 'src/app/services/patient.service';
import { VisitNoteService } from 'src/app/services/visit-note.service';

@Component({
  selector: 'app-visit-notes',
  templateUrl: './visit-notes.component.html',
  styleUrls: ['./visit-notes.component.scss']
})
export class VisitNotesComponent implements OnInit {

  public visitNotes: any = [];
  public visitNoteTest: any = null;
  showBoundaryLinks = true;
  // PAGINATION
  public pageSize = 5;
  public totalItems = 0;
  public currentPage = 1;
  public rotate = true;
  public maxSize = 5;
  public patient: any = {};

  public user: any;
  public authorities: any = [];

  constructor(
    private visitNoteService: VisitNoteService,
    private patientService: PatientService,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private authService: AuthService
  ) { 
    this.getCurrentPatient();
    this.currentUser();
  }

  ngOnInit(): void {
    this.findAll();
  }

  currentUser() {
    this.authService.getAccount().subscribe(
      (response) => {
        this.user = response;
        this.authorities = response.authorities;
      }, (error) => {
        console.log(error);
      }
    )
  }
  
  getCurrentPatient() {
    this.patientService.findOne(this.route.snapshot.params.id).subscribe(
      (response) => {
        console.log('CURRENT PATIENT', response);
        this.patient = response;
      }, (error) => {
        console.log(error);
      }
    )
  }

  findAll() {
    this.visitNoteService.findAllByPatientId(this.route.snapshot.params.id, this.currentPage - 1, this.pageSize).subscribe(
      (response) => {
        this.visitNotes = response.body;
        this.totalItems = response.headers.get('X-Total-Count');
      }, (error) => {
        console.log(error);
      }
    );
  }

  delete(specialityId: number) {
    this.visitNoteService.deleteOne(specialityId).subscribe(
      (response) => {
        console.log(response);
        this.findAll();
        this.alertService.success('The vist note was deleted successfully');
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    );
  }

  changePage(event: any): void {
    this.currentPage = event.page;
    this.findAll();
  }

  setPage(pageNo: any): void {
    this.currentPage = pageNo;
  }
  
}
