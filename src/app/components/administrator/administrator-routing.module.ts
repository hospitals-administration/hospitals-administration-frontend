import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorComponent } from './doctor/doctor.component';
import { HospitalOptionsComponent } from './hospital/hospital-options/hospital-options.component';
import { HospitalComponent } from './hospital/hospital.component';
import { VisitNoteModalComponent } from './modals/visit-note-modal/visit-note-modal.component';
import { PatientComponent } from './patient/patient.component';
import { VisitNotesComponent } from './patient/visit-notes/visit-notes.component';
import { SpecialityComponent } from './speciality/speciality.component';

const routes: Routes = [
  {
    path: 'hospitals',
    component: HospitalComponent
  },
  {
    path: 'doctors',
    component: DoctorComponent
  },
  {
    path: 'patients',
    component: PatientComponent
  },
  {
    path: 'specialities',
    component: SpecialityComponent
  },
  {
    path: 'hospital-options/:id',
    component: HospitalOptionsComponent
  },
  {
    path: 'visit-notes/patient/:id',
    component: VisitNotesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdministratorRoutingModule { }
