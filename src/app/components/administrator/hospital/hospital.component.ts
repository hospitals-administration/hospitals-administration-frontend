import { Component, OnDestroy, OnInit } from '@angular/core';
import { from } from 'rxjs';
// SERVICES
import { HospitalService } from 'src/app/services/hospital.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-hospital',
  templateUrl: './hospital.component.html',
  styleUrls: ['./hospital.component.scss']
})
export class HospitalComponent implements OnInit, OnDestroy {

  public hospitals: any = [];
  public findAllSubscription!: Subscription;
  public hospitalTest: any = null;
  // PAGINATION
  public pageSize = 5;
  public totalItems = 0;
  public currentPage = 1;
  public rotate = true;
  public maxSize = 5;
  // SEARCH
  public name: any;
  public date: any;

  public user: any;
  public authorities: any = [];

  constructor(
    private hospitalService: HospitalService,
    private authService: AuthService,
    private alertService: AlertService
  ) { 
    this.currentUser();
  }

  ngOnInit(): void {
    this.findAll();
  }

  currentUser() {
    this.authService.getAccount().subscribe(
      (response) => {
        this.user = response;
        this.authorities = response.authorities;
      }, (error) => {
        console.log(error);
      }
    )
  }

  findAll() {
    this.findAllSubscription = this.hospitalService.findAll(this.currentPage - 1, this.pageSize).subscribe(
      (response) => {
        this.hospitals = response.body;
        this.totalItems = response.headers.get('X-Total-Count');
      }, (error) => {
        console.log(error);
      }
    );
  }

  delete(hospitalId: number) {
    this.hospitalService.deleteOne(hospitalId).subscribe(
      (response) => {
        console.log(response);
        this.findAll();
      }, (error) => {
        console.log(error);
      }
    );
  }

  changePage(event: any): void {
    this.currentPage = event.page;
    this.findAll();
  }

  setPage(pageNo: any): void {
    this.currentPage = pageNo;
  }

  ngOnDestroy(): void {
    this.findAllSubscription.unsubscribe();
  }

  // SEARCH
  search() {
    this.hospitalService.search(this.name, this.date, this.currentPage - 1, this.pageSize).subscribe(
      (response) => {
        this.hospitals = response.body;
        this.totalItems = response.headers.get('X-Total-Count');
        const data = response.body.length === 0 ? 'No Results' : response.body.length + ' hospitals';
        this.alertService.success('Successful search : ' + data);
      }, (error) => {
        console.log(error);
      }
    );
  }
  
  clear() {
    this.name = undefined;
    this.date = undefined;
    this.findAll()
  }

}
