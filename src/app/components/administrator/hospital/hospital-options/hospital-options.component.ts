import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';
import { DoctorService } from 'src/app/services/doctor.service';

@Component({
  selector: 'app-hospital-options',
  templateUrl: './hospital-options.component.html',
  styleUrls: ['./hospital-options.component.scss']
})
export class HospitalOptionsComponent implements OnInit, OnDestroy {

  public doctors: any = [];
  public findAllSubscription!: Subscription;
  public doctorTest: any = null;
  // PAGINATION
  public pageSize = 5;
  public totalItems = 0;
  public currentPage = 1;
  public rotate = true;
  public maxSize = 5;
  public hospitalId = null;
  
  constructor(
    private doctorService: DoctorService,
    private route: ActivatedRoute,
    private alertService: AlertService,
  ) { 
    this.hospitalId = this.route.snapshot.params.id;
  }

  ngOnInit(): void {
    this.findAllDoctorsByHospitalId();
  }

  findAllDoctorsByHospitalId() {
    this.findAllSubscription = this.doctorService.findAllByHospitalId(this.currentPage - 1, this.pageSize, this.route.snapshot.params.id).subscribe(
      (response) => {
        console.log(response)
        this.doctors = response.body;
        this.totalItems = response.headers.get('X-Total-Count');
      }, (error) => {
        console.log(error);
      }
    );
  }

  delete(hospitalId: number) {
    this.doctorService.deleteOne(hospitalId).subscribe(
      (response) => {
        this.findAllDoctorsByHospitalId();
        this.alertService.success('The doctor was deleted successfully');
      }, (error) => {
        this.alertService.error(error.error.title);
      }
    );
  }

  changePage(event: any): void {
    this.currentPage = event.page;
    this.findAllDoctorsByHospitalId();
  }

  setPage(pageNo: any): void {
    this.currentPage = pageNo;
  }

  ngOnDestroy(): void {
    this.findAllSubscription.unsubscribe();
  }

}
