import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HospitalOptionsComponent } from './hospital-options.component';

describe('HospitalOptionsComponent', () => {
  let component: HospitalOptionsComponent;
  let fixture: ComponentFixture<HospitalOptionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HospitalOptionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HospitalOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
