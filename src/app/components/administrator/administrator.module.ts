import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministratorRoutingModule } from './administrator-routing.module';
import { DoctorComponent } from './doctor/doctor.component';
import { PatientComponent } from './patient/patient.component';
import { HospitalComponent } from './hospital/hospital.component';
import { SpecialityComponent } from './speciality/speciality.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ModalsModule } from './modals/modals.module';
import { HospitalOptionsComponent } from './hospital/hospital-options/hospital-options.component';
import { ViewNotesComponent } from './patient/view-notes/view-notes.component';
import { VisitNotesComponent } from './patient/visit-notes/visit-notes.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
@NgModule({
  declarations: [
    DoctorComponent,
    PatientComponent,
    HospitalComponent,
    SpecialityComponent,
    HospitalOptionsComponent,
    ViewNotesComponent,
    VisitNotesComponent,
  ],
  imports: [
    CommonModule,
    AdministratorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    TooltipModule.forRoot(),
    ModalsModule
  ]
})
export class AdministratorModule { }
