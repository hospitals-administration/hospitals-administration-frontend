import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public userFormGroup!: FormGroup;
  public submitted: any = false;
  
  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private alertService: AlertService
  ) { 
  }

  ngOnInit(): void {
    this.validateFormGroup();
  }

  validateFormGroup() {
    this.userFormGroup = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(3)]],
      rememberMe: [true]
    });
  }

  get formControls() {
    return this.userFormGroup.controls;
  }

  get formValue() {
    return this.userFormGroup.value;
  }

  onSubmit() {
    this.submitted = true;
    if (this.userFormGroup.invalid) {
      return;
    }
    this.login();
  }

  login() {
    this.authService.login(this.formValue).subscribe(
      (response) => {
        this.logout();
        if (this.formValue.rememberMe) {
          localStorage.setItem('hospital-token', response.id_token);
        } else {
          sessionStorage.setItem('hospital-token', response.id_token);
        }
        this.authService.getAccount().subscribe(
          (responseTwo) => {
            const data = responseTwo.authorities;
            if (data.includes("ROLE_PATIENT")) {
              this.router.navigate(['/admin/visit-notes/patient/' + responseTwo.id]);
            } else {
              this.router.navigate(['/admin/hospitals']);
            }
          }, (errorTwo) => {
            console.log(errorTwo);
          }
        );
        this.alertService.success('The hospital was created successfully');
      },
      (error) => {
        console.log(error);
        this.alertService.error(error.error.detail);
      }
    );
  }

  logout() {
    this.authService.logout();
  }

}
