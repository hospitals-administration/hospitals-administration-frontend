import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// COMPONENTS
import { ContentComponent } from './components/content/content.component';
import { LoginComponent } from './components/login/login.component';
// GUARDS
import { AuthGuard } from './guards/auth-guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent,   
  },
  {
    path: 'admin',
    component: ContentComponent,
    canActivate: [AuthGuard],
    children: [{
      path: '',
      loadChildren: () => import('./components/administrator/administrator.module').then(m => m.AdministratorModule)
    }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
