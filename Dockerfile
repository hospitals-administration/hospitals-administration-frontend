# NODE VERSION
FROM node:12.20.2-alpine AS builder
COPY . ./hospitals-administration-frontend
WORKDIR /hospitals-administration-frontend
RUN npm install
RUN npm run build -- --prod
# NGINX VESION
FROM nginx:1.19-alpine
COPY --from=builder /hospitals-administration-frontend/dist/hospitals-administration-frontend /usr/share/nginx/html
COPY --from=builder /hospitals-administration-frontend/nginx.conf /etc/nginx/conf.d/default.conf